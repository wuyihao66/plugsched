# Sidecar
由于 Linux 内核调度与其他子系统紧耦合，因此修改调度器往往也要少量修改其他子系统。比如，

- 如果想对组调度（Group Scheduling）做一些统计工作，可能需要同时修改 cpuacct.c。
- 如果想修改内核线程的调度策略，你可能也会修改 kthread.c。
- 如果想修改 CPU 亲和性相关策略，可能还需要修改 cpuset.c。

Sidecar 复用了很多 plugsched 的基础设施，因此 sidecar 用起来几乎跟 plugsched 的核心功能一样简单。

### How it works
sidecar 的开发只需要在一个文件里进行 `kernel/sched/mod/sidecar.c` 。

开发好这个文件之后，就可以触发标准的编译和打包。参考 [Quick Start](../README_zh.md#quick-start)。Plugsched 便会去编译这个文件，然后把编译结果链接到ko里。

接下来，在安装调度器模块时，类似于 [编译及安装调度器](../README_zh.md#编译及安装调度器) 中提到的功能，都会为 sidecar 函数也执行一遍。（除调度器状态重建外）

### 例子
这个例子也可以在`/path/to/plugsched/examples/`中找到。
1. 登陆云服务器后，先安装一些必要的基础软件包
2. 创建临时工作目录，下载系统内核的 SRPM 包
3. 启动并进入容器
4. 解压内核源代码
5. 进行边界划分与提取
步骤1~5 详细操作参考[Quick Start](../README_zh.md#quick-start).

6. 测试 sidecar 功能

把下面这行粘贴进文件 `kernel/sched/mod/export_jump_sidecar.h`

    EXPORT_SIDECAR(name_to_int, fs/proc/util.c, unsigned, const struct qstr *)

把下面这段粘贴进文件 `kernel/sched/mod/sidecar.c`
```c
/**
 * Copyright 2019-2022 Alibaba Group Holding Limited.
 * SPDX-License-Identifier: GPL-2.0 OR BSD-3-Clause
 */

/*
 * Copied from ./fs/proc/util.c and did a little modification to it
 */
#include <linux/dcache.h>

unsigned name_to_int(const struct qstr *qstr)
{
  const char *name = qstr->name;
  int len = qstr->len;
  unsigned n = 0;
  trace_printk("%s\n", name);      /* !! We added this line !! */

  if (len > 1 && *name == '0')
    goto out;
  do {
    unsigned c = *name++ - '0';
    if (c > 9)
      goto out;
    if (n >= (~0U-9)/10)
      goto out;
    n *= 10;
    n += c;
  } while (--len > 0);
  return n;
  out:
  return ~0U;
}
```

7. 打包生成 rpm 包

``` shell
# plugsched-cli build /tmp/work/scheduler
```

8. 将生成的 rpm 包拷贝到宿主机，退出容器，安装调度器
``` shell
rpm -ivh /tmp/work/scheduler-xxx-4.19.91-25.2.an7.yyy.x86_64.rpm
```
# Mempool

### 为什么需要mempool

plugsched的用户很可能会有修改结构体的需求。例如，在结构体中增加、删除、替换一个成员。但plugsched并不支持修改结构体大小、布局。

**删除**
如果用户希望删除一个成员，则应该只删除它被引用的地方代码，而不应该删除结构体中的成员定义，以保持结构体大小不变。

**替换**
我们不建议替换成员，建议删除并增加新成员。以降低误用的风险。

**增加**
如果用户希望增加一个成员，则应该保证内核源代码中该结构体中至少有一个**保留字段**，例如
```c
struct my_struct {
    int field1;
    u64 reserved1;
    u64 reserved2;
}
```
用户便可以使用reserved1或reserved2存放新增加的成员，以保持结构体大小不变。如果用户需要增加不止2个成员怎么办？这就是mempool引入的原因。

### How it works
```plain
用户定义要在哪个父亲结构体（task_struct或sched_entity等）中，
以指针的形式嵌入什么新的子结构体，
    ↓
加载模块
    ↓
按需统计对象的数量。（O(1)复杂度) <------------------------------------+
    ↓                                                             |
该数量乘以一个系数再乘以sizeof(子结构体)，分配mempool。                  |
    ↓                                                             |
stop_machine                                                      |
    ↓                                    Y                        |
重新统计数量，数量超出预留内存？---------------------------------------+
    ↓ N
利用所有CPU，遍历所有对象 (O(N/CPU)复杂度，N是对象数量，例如进程数量)
将mempool分成N块，每块的大小为sizeof(子结构体)
    ↓
加载完成

进程创建                 进程销毁
    ↓                     ↓
kmalloc分配新的        如果是从mempool中分配出来的，则还给mempool → 如果mempool还完则释放mempool
子结构体               如果是从kmalloc出来的，则直接kfree

```

### 例子
我们以task\_struct为例，假如用户希望在task\_struct增加一个enhance\_task结构。

注意，`nr_threads + nr_cpu_ids`，表示普通线程数量 + idle线程数量

注意文件 kernel/sched/mod/mempool.h，该文件其实是完全空白的，其中只包括一些Example，并且被注释掉了。
而下面的diff，则参照着注释，将mempool.h填充上了。

```diff
diff --git a/include/linux/sched.h b/include/linux/sched.h
index 8055f43d8..9a31143f9 100644
--- a/include/linux/sched.h
+++ b/include/linux/sched.h
@@ -625,6 +625,12 @@ struct wake_q_node {
 	struct wake_q_node *next;
 };

+struct enhance_task {
+	u64 val1;
+	u64 val2;
+	u64 val3;
+};
+
 struct task_struct {
 #ifdef CONFIG_THREAD_INFO_IN_TASK
 	/*
@@ -1268,7 +1274,7 @@ struct task_struct {

 	struct callback_head		mce_kill_me;
 #endif
-	CK_HOTFIX_RESERVE(1)
+	struct enhance_task *et;
 	CK_HOTFIX_RESERVE(2)
 	CK_HOTFIX_RESERVE(3)
 	CK_HOTFIX_RESERVE(4)
diff --git a/kernel/sched/mod/Makefile b/kernel/sched/mod/Makefile
index 3e4d48a77..d11652cea 100644
--- a/kernel/sched/mod/Makefile
+++ b/kernel/sched/mod/Makefile
@@ -41,1 +41,1 @@ obj-stubs := $(patsubst %.o,%.stub.o,$(objs-y))
-ccflags-n += -DSCHEDMOD_MEMPOOL
+ccflags-y += -DSCHEDMOD_MEMPOOL
diff --git a/kernel/sched/mod/core.c b/kernel/sched/mod/core.c
index f3376073c..99fda425e 100644
--- a/kernel/sched/mod/core.c
+++ b/kernel/sched/mod/core.c
@@ -1999,6 +1999,8 @@ int wake_up_state(struct task_struct *p, unsigned int state)
 static void __sched_fork(unsigned long clone_flags, struct task_struct *p)
 {
 	p->on_rq			= 0;
+	p->et                           = kzalloc(sizeof(struct enhance_task), GFP_KERNEL);
+	p->et->val1                     = p->comm[0];

 	p->se.on_rq			= 0;
 	p->se.exec_start		= 0;
diff --git a/kernel/sched/mod/export_jump_sidecar.h b/kernel/sched/mod/export_jump_sidecar.h
index 6cdadeedd..04601eac6 100644
--- a/kernel/sched/mod/export_jump_sidecar.h
+++ b/kernel/sched/mod/export_jump_sidecar.h
@@ -6,3 +6,4 @@
  * from scheduler functions. Sidecar helps to do this.
  * See /path/to/plugsched/examples/export_jump_sidecar.h for usage examples.
  */
+EXPORT_SIDECAR(release_thread, arch/x86/kernel/process_64.c, void, struct task_struct *)
diff --git a/kernel/sched/mod/mempool.h b/kernel/sched/mod/mempool.h
index 24d18d374..92c080e04 100644
--- a/kernel/sched/mod/mempool.h
+++ b/kernel/sched/mod/mempool.h
@@ -103,6 +103,12 @@ static int recheck_mempool_##name(void)					\
  * 		nr_cpu_ids);	// we alloc nr_cpu_ids objects before stop_machine
  */

+DEFINE_RESERVE(task_struct,     // struct task_struct
+               et,              // struct task_struct's et field
+               et,              // name the mempool as et
+               nr_threads + nr_cpu_ids,      // we need exactly nr_threads + nr_cpu_ids objects,
+               (nr_threads + nr_cpu_ids)*2)  // we addloc twice as many before stop_machine
+
 static int sched_mempools_create(void)
 {
 	int err;
@@ -116,6 +122,9 @@ static int sched_mempools_create(void)
 	 * 	return err;
 	 */

+	if ((err = create_mempool_et()))
+		return err;
+
 	return 0;
 }

@@ -126,6 +135,8 @@ static void sched_mempools_destroy(void)
 	 * simple_mempool_destroy(se_smp);
 	 * simple_mempool_destroy(rq_smp);
 	 */
+
+	simple_mempool_destroy(et_smp);
 }

 static int recheck_smps(void)
@@ -141,6 +152,8 @@ static int recheck_smps(void)
 	 * 	return err;
 	 */

+	if ((err = recheck_mempool_et()))
+		return err;
 	return 0;
 }

@@ -161,7 +174,16 @@ static void sched_alloc_extrapad(void)

 	 * for_each_process_thread(p, t)
 	 * 	t->se.statistics.bvt = alloc_se_reserve();
-
+	 */
+	int cpu;
+	struct task_struct *p, *t;
+	for_each_process_thread(p, t) {
+		t->et = alloc_et_reserve();
+		t->et->val1 = t->comm[0];
+	}
+	for_each_possible_cpu(cpu)
+		idle_task(cpu)->et = alloc_et_reserve();
+	/*
 	 * list_for_each_entry_rcu(tg, &task_groups, list) {
 	 * 	if (tg == &root_task_group || task_group_is_autogroup(tg))
 	 *		continue;
@@ -185,9 +207,14 @@ static void sched_free_extrapad(void)
 	 * 	release_se_reserve(&idle_task(cpu)->se.statistics);
 	 * 	release_rq_reserve(cpu_rq(cpu));
 	 * }
-	 * for_each_process_thread (p, t)
-	 * 	release_se_reserve(&t->se.statistics);
-
+	 */
+	int cpu;
+	struct task_struct *p, *t;
+	for_each_process_thread (p, t)
+		release_et_reserve(t);
+	for_each_possible_cpu(cpu)
+		release_et_reserve(idle_task(cpu));
+	/*
 	 * list_for_each_entry_rcu(tg, &task_groups, list) {
 	 * 	if (tg == &root_task_group || task_group_is_autogroup(tg))
 	 *		continue;
diff --git a/kernel/sched/mod/sidecar.c b/kernel/sched/mod/sidecar.c
index 21c45a6b2..eabc74410 100644
--- a/kernel/sched/mod/sidecar.c
+++ b/kernel/sched/mod/sidecar.c
@@ -8,3 +8,18 @@
  * from scheduler functions. Sidecar helps to do this.
  * See /path/to/plugsched/examples/sidecar.c for usage examples.
  */
+#include "sched.h"
+
+#define MCOUNT_SIZE 5
+#define ORIG_FUNC(f) (*(typeof(&(f)))((unsigned long)__vmlinux__##f + MCOUNT_SIZE))
+
+extern void release_et_reserve(struct task_struct*);
+extern void __vmlinux__release_thread(struct task_struct*);
+
+void release_thread(struct task_struct *dead_task)
+{
+	printk("%s %d\n", dead_task->comm, dead_task->et->val1);
+	release_et_reserve(dead_task);
+
+	ORIG_FUNC(release_thread)(dead_task);
+}
```
